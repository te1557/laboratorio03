package com.academiamoviles.laboratorio03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_result.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSend.setOnClickListener{
            val name = edtName.text.toString()
            val age = edtAge.text.toString()
            val pet =   if(rbDog.isChecked) {
                            "Perro"
                        } else if(rbCat.isChecked){
                            "Gato"
                        } else if(rbRabbit.isChecked) {
                            "Conejo"
                        }else{
                            ""
                        }

            val vaccine1 =   if(chkVaccine1.isChecked) "V1" else ""
            val vaccine2 =   if(chkVaccine2.isChecked) "V2"  else ""
            val vaccine3 =   if(chkVaccine3.isChecked) "V3"  else ""
            val vaccine4 =   if(chkVaccine4.isChecked) "V4"  else ""
            val vaccine5 =   if(chkVaccine5.isChecked) "V5"  else ""

            if(name.isEmpty()){
                toast("Debe ingresar sus nombres")
                return@setOnClickListener
            }

            if(age.isEmpty()){
                toast("Debe ingresar su edad")
                return@setOnClickListener
            }

            if(pet.isEmpty()){
                toast("Debe seleccionar una mascota")
                return@setOnClickListener
            }

            if(vaccine1.isEmpty() && vaccine2.isEmpty()&& vaccine3.isEmpty()&& vaccine4.isEmpty()&& vaccine5.isEmpty()){
                toast("Debe seleccionar una vacuna")
                return@setOnClickListener
            }

            val bundle = Bundle().apply {
                putString("KEY_NAME",name)
                putString("KEY_AGE",age)
                putString("KEY_PET",pet)
                putString("KEY_VAC1",vaccine1)
                putString("KEY_VAC2",vaccine2)
                putString("KEY_VAC3",vaccine3)
                putString("KEY_VAC4",vaccine4)
                putString("KEY_VAC5",vaccine5)
            }

            val intent = Intent(this,ResultActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
            finish()



        }


    }

    fun toast(message:String) = Toast.makeText(this,message, Toast.LENGTH_SHORT).show()


}