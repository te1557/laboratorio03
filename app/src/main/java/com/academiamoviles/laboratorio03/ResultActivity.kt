package com.academiamoviles.laboratorio03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val bundleResult = intent.extras

        bundleResult?.let { bundle ->
            val name = bundle.getString("KEY_NAME") ?: "Desconocido"
            val age = bundle.getString("KEY_AGE") ?: "Desconocido"
            val pet = bundle.getString("KEY_PET") ?:  ""
            val vac1 = bundle.getString("KEY_VAC1") ?:  ""
            val vac2 = bundle.getString("KEY_VAC2") ?:  ""
            val vac3 = bundle.getString("KEY_VAC3") ?:  ""
            val vac4 = bundle.getString("KEY_VAC4") ?:  ""
            val vac5 = bundle.getString("KEY_VAC5") ?:  ""

            tvName.text = name
            tvAge.text = age

            if(!pet.isEmpty()){
                val image : Int
                if(pet.equals("Perro")){
                    rbDogResult.setChecked(true)
                    image = R.drawable.dog
                }else if(pet.equals("Gato")){
                    rbCatResult.setChecked(true)
                    image = R.drawable.cat
                }else if(pet.equals("Conejo")){
                    rbRabbitResult.setChecked(true)
                    image = R.drawable.rabbit
                }else{
                    image = 0
                }
                if(image!=0) imgPet.setImageResource(image) else imgPet.visibility= View.GONE
            }

            if(!vac1.isEmpty() || !vac2.isEmpty() || !vac3.isEmpty()|| !vac4.isEmpty()|| !vac5.isEmpty()){
                if(vac1.equals("V1")){
                    chkVaccine1Result.setChecked(true)
                }
                if(vac2.equals("V2")){
                    chkVaccine2Result.setChecked(true)
                }
                if(vac3.equals("V3")){
                    chkVaccine3Result.setChecked(true)
                }
                if(vac4.equals("V4")){
                    chkVaccine4Result.setChecked(true)
                }
                if(vac5.equals("V5")){
                    chkVaccine5Result.setChecked(true)
                }
            }

        }

        btnReturn.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}